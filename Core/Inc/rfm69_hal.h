/**
 * @file rfm69_hal.h
 * @brief Brief_description
 *
 * @date Apr 16, 2020
 * @author drid
 */

#ifndef INC_RFM69_HAL_H_
#define INC_RFM69_HAL_H_

#include <rfm69.h>
#include "stm32l1xx_hal.h"

extern SPI_HandleTypeDef hspi1;

#define RF_NSS_Pin GPIO_PIN_0
#define RF_NSS_GPIO_Port GPIOB
#define RF_INT_Pin GPIO_PIN_1
#define RF_INT_GPIO_Port GPIOB
#define RF_RST_Pin GPIO_PIN_2
#define RF_RST_GPIO_Port GPIOB

#define RF_FREQ		433000000U
#define RF_FREQ_DEV	4800U
#define RF_BITRATE	9600

#define RF_FREQ_MSB ((uint32_t)(RF_FREQ/RFM69_FSTEP) & 0xFF0000)>>16
#define RF_FREQ_MID ((uint32_t)(RF_FREQ/RFM69_FSTEP) & 0x00FF00)>>8
#define RF_FREQ_LSB ((uint32_t)(RF_FREQ/RFM69_FSTEP) & 0xFF)

#define RF_FREQ_DEV_MSB ((uint32_t)(RF_FREQ_DEV/RFM69_FSTEP) & 0xFF00)>>8
#define RF_FREQ_DEV_LSB ((uint32_t)(RF_FREQ_DEV/RFM69_FSTEP) & 0xFF)+1

#define RF_BITRATE_MSB ((uint32_t)(RFM69_FXOSC/RF_BITRATE) & 0xFF00)>>8
#define RF_BITRATE_LSB ((uint32_t)(RFM69_FXOSC/RF_BITRATE) & 0xFF)+1

uint8_t rfm69_setup(struct rfm69_dev *device);

#endif /* INC_RFM69_HAL_H_ */
