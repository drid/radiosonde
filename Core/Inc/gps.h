/**
 * @file gps.h
 * @brief Brief_description
 *
 * @date May 7, 2020
 * @author Ilias Daradimos
 */

#ifndef INC_GPS_H_
#define INC_GPS_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

/**
 * @brief GPS data structure
 */
struct gps_data {
	char time[11];		//!< Time in HHmmss format
	bool fix;			//!< gps fix validity
	bool power;			//!< GPS power On
	float speed;		//!< Speed over ground in knots
	char date[9];		//!< Date
	float lat;			//!< Latitude in decimal dergrees
	float lon;			//!< Longitude in decimal degrees
	float altitude;		//!< Altitude in meters
	float true_course;	//!< True course
};

void gps_parse_rmc(char*, struct gps_data*);
void gps_parse_gga(char* nmea, struct gps_data* gps_data);
void gps2time(struct gps_data*, struct tm*);

#endif /* INC_GPS_H_ */
