/**
 * @file rfm69_hal.c
 * @brief Brief_description
 *
 * @date Apr 18, 2020
 * @author drid
 */

#include "rfm69_hal.h"

void rfm69_chip_select(FlagStatus state);
void rfm69_chip_reset(FlagStatus state);
HAL_StatusTypeDef rfm69_spi_read(uint8_t *data, uint16_t len);
HAL_StatusTypeDef rfm69_spi_write(uint8_t *data, uint16_t len);
HAL_StatusTypeDef rfm69_spi_transfer(uint8_t *pTxData, uint8_t *pRxData,
		uint16_t Size);

/** RFM69 base configuration after init().
 *
 * Change these to your needs or call rfm69_setCustomConfig() after module init.
 */
static uint8_t rfm69_base_config[][2] =
{
	    {0x01, 0x00}, // RegOpMode: Sleep Mode
	    {0x02, 0x03}, // RegDataModul: Packet mode, FSK,  Gaussian filter, BT = 0.3
	    {0x03, RF_BITRATE_MSB}, // RegBitrateMsb: 9.6 kbps
	    {0x04, RF_BITRATE_LSB}, // RegBitrateLsb
	    {0x05, RF_FREQ_DEV_MSB}, // RegFdevMsb: 20 kHz
	    {0x06, RF_FREQ_DEV_LSB}, // RegFdevLsb
	    {0x07, RF_FREQ_MSB}, // RegFrfMsb: 433 MHz
	    {0x08, RF_FREQ_MID}, // RegFrfMid
	    {0x09, RF_FREQ_LSB}, // RegFrfLsb
	    {0x18, 0x88}, // RegLNA: 200 Ohm impedance, gain set by AGC loop
	    {0x19, 0x4C}, // RegRxBw: 25 kHz
	    {0x2C, 0x00}, // RegPreambleMsb: 8 bytes preamble
	    {0x2D, 0x08}, // RegPreambleLsb
	    {0x2E, 0xC8}, // RegSyncConfig: Enable sync word, 2 bytes sync word
	    {0x2F, 0x7A}, // RegSyncValue1: 0x7A0E
	    {0x30, 0x0E}, // RegSyncValue2
	    {0x37, 0x58}, // RegPacketConfig1: Fixed length, CRC on, whitening
	    {0x38, 52}, // RegPayloadLength: 64 bytes max payload
		{0x3B, 0x5B}, // RegAutoModes
	    {0x3C, 51}, // RegFifoThresh: TxStart on FifoNotEmpty, 63 bytes FifoLevel
	    {0x58, 0x1B}, // RegTestLna: Normal sensitivity mode
	    {0x6F, 0x30}, // RegTestDagc: Improved margin, use if AfcLowBetaOn=0 (default)
};

uint8_t rfm69_setup(struct rfm69_dev *device) {
	device->is_high_power = true;
	device->read = (rfm69_spi_fptr_t) rfm69_spi_read;
	device->write = (rfm69_spi_fptr_t) rfm69_spi_write;
	device->chip_reset = rfm69_chip_reset;
	device->chip_select = rfm69_chip_select;
	device->delay_ms = HAL_Delay;
	device->get_tick = HAL_GetTick;
	device->conf = (uint8_t*)rfm69_base_config;
	device->conf_size = sizeof(rfm69_base_config)/2;
	device->int_packet_sent = true;
	return 0;
}

void rfm69_chip_select(FlagStatus state) {
	HAL_GPIO_WritePin(RF_NSS_GPIO_Port, RF_NSS_Pin,
			(state == RESET ? GPIO_PIN_SET : GPIO_PIN_RESET));
}

void rfm69_chip_reset(FlagStatus state) {
	HAL_GPIO_WritePin(RF_RST_GPIO_Port, RF_RST_Pin,
			(state == RESET ? GPIO_PIN_SET : GPIO_PIN_RESET));
}

/**
 * @brief RFM69 SPI read function.
 *
 * @param reg The register to be read
 * @param data Pointer to data
 * @param len Length of data
 * @return HAL status
 */
HAL_StatusTypeDef rfm69_spi_read(uint8_t *data, uint16_t len) {
	HAL_StatusTypeDef status;
	status = HAL_SPI_Receive(&hspi1, data, len, 500);
	return status;
}

/**
 * @brief RFM69 SPI write function
 *
 * @param reg The register to be read
 * @param data Pointer to data
 * @param len Length of data
 * @return HAL status
 */
HAL_StatusTypeDef rfm69_spi_write(uint8_t *data, uint16_t len) {
	HAL_StatusTypeDef status;
	status = HAL_SPI_Transmit(&hspi1, data, len, 500);
	return status;
}
