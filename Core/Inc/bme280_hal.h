/**
 * @file	bme280_wrap.h
 * @brief Header for bme280_wrap.c library wrapper
 */

#ifndef BMP280_DRIVER_BMP280_SUPPORT_H_
#define BMP280_DRIVER_BMP280_SUPPORT_H_

#include <bme280.h>
#include "stm32l1xx_hal.h"

extern I2C_HandleTypeDef hi2c1; /** external I2C pointer */

int8_t bme280_setup();
int8_t bme280_read_sensor_data_normal_mode(struct bme280_data *comp_data);
#endif /* BMP280_DRIVER_BMP280_SUPPORT_H_ */
