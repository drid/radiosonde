/**
 * @file radiosonde.h
 * @brief Brief_description
 *
 * @date Apr 21, 2020
 * @author Ilias Daradimos
 */

#ifndef INC_RADIOSONDE_H_
#define INC_RADIOSONDE_H_

struct rs_status{
		uint8_t GPSFix :1;
		uint8_t SHT_Active :1;
		uint8_t BME_Active :1;
	};

struct radiosonde_packet {
	uint16_t id;
	uint16_t battery; /*!< Battery voltage in millivots */
	uint32_t uptime; /*!< Uptime in ms */
	int16_t temperature_int; /*!< Temperature in C * 10 */
	float temperature_ext;
	float humidity_ext;
	uint8_t humidity_int; /*!< Humidity % */
	uint16_t pressure_int; /*!< Pressure in hPa */
	struct rs_status status;
	uint32_t timestamp; /*!< Unix timestamp */
	float lat; /*!< Latitude in decimal degrees */
	float lon; /*!< logitude in decimal degrees */
	float altitude; /*!< Altitude in meters */
	float speed;
	float true_heading;
};

#endif /* INC_RADIOSONDE_H_ */
