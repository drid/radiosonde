/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define GPS_EN_Pin GPIO_PIN_1
#define GPS_EN_GPIO_Port GPIOA
#define RF_NSS_Pin GPIO_PIN_0
#define RF_NSS_GPIO_Port GPIOB
#define RF_INT_Pin GPIO_PIN_1
#define RF_INT_GPIO_Port GPIOB
#define RF_INT_EXTI_IRQn EXTI1_IRQn
#define RF_RST_Pin GPIO_PIN_2
#define RF_RST_GPIO_Port GPIOB
#define ENC_SCL_Pin GPIO_PIN_10
#define ENC_SCL_GPIO_Port GPIOB
#define ENV_SDA_Pin GPIO_PIN_11
#define ENV_SDA_GPIO_Port GPIOB
#define SHT_Reset_Pin GPIO_PIN_14
#define SHT_Reset_GPIO_Port GPIOB
#define SHT_Alert_Pin GPIO_PIN_15
#define SHT_Alert_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define TX_INTERVAL 1000
#define EPOCH		2208988800
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
