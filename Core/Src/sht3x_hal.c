/**
 * @file sht3x_hal.c
 * @brief Brief_description
 *
 * @date Apr 18, 2020
 * @author Ilias Daradimos
 */

#include "sht3x_hal.h"

uint8_t SHT3x_i2c_read(uint8_t dev_id, uint16_t reg_addr, uint8_t *reg_data,
		uint16_t len);
uint8_t SHT3x_i2c_write(uint8_t dev_id, uint16_t cmd);
void SHT3x_reset();

void SHT3x_setup(struct sht3x_dev *sensor) {
	sensor->address = SHT3x_ADDRESS;
	sensor->read = SHT3x_i2c_read;
	sensor->write = SHT3x_i2c_write;
	sensor->chip_reset = SHT3x_reset;
	sensor->mode = SHT3x_CMD_MEAS_CLOCKSTR_L;
//	sensor->mode = SHT3x_CMD_MEAS_PERI_05_L;
}

/**
 * @brief I2C read implementation
 *
 * This function is called from SHT3x driver when it needs to write to the device
 * @param dev_id SHT3x device id
 * @param reg_addr Read address
 * @param reg_data Holds the data returned from read operation
 * @param len Length of data to read
 * @return uint8_t status
 */
uint8_t SHT3x_i2c_read(uint8_t dev_id, uint16_t cmd, uint8_t *data,
		uint16_t len) {
	HAL_StatusTypeDef status = HAL_OK;
	status = HAL_I2C_IsDeviceReady(&hi2c2, (uint16_t) (dev_id << 1), 3, 1000);
	if (status != HAL_OK) {
		switch (status) {
		case HAL_TIMEOUT:
			return SHT_TIMEOUT_ERROR;
			break;
		default:
			return SHT_ERROR;
			break;
		}
	}

	status = HAL_I2C_Mem_Read(&hi2c2, (dev_id << 1), cmd, I2C_MEMADD_SIZE_16BIT,
			data, len, 1000);

	if (status != HAL_OK) {
		switch (status) {
		case HAL_TIMEOUT:
			return SHT_TIMEOUT_ERROR;
			break;
		default:
			return SHT_ERROR;
			break;
		}
	}
	return SHT_OK;
}

/**
 * @brief I2C write implementation
 *
 * This function is called from SHT3x driver when it needs to write to the device
 * @param dev_id SHT3x device id
 * @param reg_addr Read address
 * @param reg_data Data to be sent to the device
 * @param len Length of data to send
 * @return
 */
uint8_t SHT3x_i2c_write(uint8_t dev_id, uint16_t cmd) {
	HAL_StatusTypeDef status = HAL_OK;
	uint8_t buf[2];

	buf[0] = cmd >> 8;
	buf[1] = cmd;
	status = HAL_I2C_IsDeviceReady(&hi2c2, (uint16_t) (dev_id << 1), 3, 1000);
	if (status != HAL_OK) {
		switch (status) {
		case HAL_TIMEOUT:
			return SHT_TIMEOUT_ERROR;
			break;
		default:
			return SHT_ERROR;
			break;
		}
	}

	status = HAL_I2C_Master_Transmit(&hi2c2, dev_id << 1, buf, 2, 1000);
	if (status != HAL_OK) {
		switch (status) {
		case HAL_TIMEOUT:
			return SHT_TIMEOUT_ERROR;
			break;
		default:
			return SHT_ERROR;
			break;
		}
	}
	HAL_Delay(1);
	return SHT_OK;
}

/**
 * @brief Reset implementation
 */
void SHT3x_reset() {
	HAL_GPIO_WritePin(SHT_Reset_GPIO_Port, SHT_Reset_Pin, GPIO_PIN_RESET);
	HAL_Delay(50);
	HAL_GPIO_WritePin(SHT_Reset_GPIO_Port, SHT_Reset_Pin, GPIO_PIN_SET);
}
