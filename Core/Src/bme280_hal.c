/**
 * @file	bme280_wrap.c
 * @brief 	Wrapper for BME280 library
 */
#include "bme280_hal.h"

struct bme280_dev bme280;

/**
 * @brief I2C read implementation
 *
 * This function is called from BME280 driver when it needs to write to the device
 * @param dev_id BME280 device id
 * @param reg_addr Read address
 * @param reg_data Holds the data returned from read operation
 * @param len Length of data to read
 * @return uint8_t status
 */
int8_t bme280_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data,
		uint16_t len) {
	HAL_StatusTypeDef status = HAL_OK;
	if (HAL_I2C_IsDeviceReady(&hi2c1, (uint16_t) (dev_id << 1), 3, 1000)
			!= HAL_OK) {
		return 1;
	}

	status = HAL_I2C_Mem_Read(&hi2c1, (uint8_t) (dev_id << 1),
			(uint8_t) reg_addr, I2C_MEMADD_SIZE_8BIT, reg_data, len, 1000);

	if (status != HAL_OK) {
		return 1;
	}
	return 0;
}

/**
 * @brief I2C write implementation
 *
 * This function is called from BME280 driver when it needs to write to the device
 * @param dev_id BME280 device id
 * @param reg_addr Read address
 * @param reg_data Data to be sent to the device
 * @param len Length of data to send
 * @return
 */
int8_t bme280_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data,
		uint16_t len) {
	HAL_StatusTypeDef status = HAL_OK;

	if (HAL_I2C_IsDeviceReady(&hi2c1, (dev_id << 1), 3, 1000) != HAL_OK) {
		return 1;
	}

	status = HAL_I2C_Mem_Write(&hi2c1, (dev_id << 1), reg_addr,
			I2C_MEMADD_SIZE_8BIT, reg_data, len, 1000);
//	TODO: Find a fix to remove this delay
	HAL_Delay(40);
	if (status != HAL_OK) {
		return 1;
	}
	return 0;
}

/**
 * @brief Delay implementation
 *
 * Implementation of delay in milliseconds. It is called from BME280 driver
 * @param millisec Time to delay in milliseconds
 */
void bme280_delay_ms(uint32_t millisec) {
	HAL_Delay(millisec);
}

/**
 * @brief Setup BME280 device parameters
 * @return BME280 status
 */
int8_t bme280_setup() {
	int8_t rslt = BME280_OK;

	if (HAL_I2C_IsDeviceReady(&hi2c1, (BME280_I2C_ADDR_PRIM << 1), 3, 1000) == HAL_OK) {
		bme280.dev_id = BME280_I2C_ADDR_PRIM;
	} else {
		bme280.dev_id = BME280_I2C_ADDR_SEC;
	}
	bme280.intf = BME280_I2C_INTF;
	bme280.read = bme280_i2c_read;
	bme280.write = bme280_i2c_write;
	bme280.delay_ms = bme280_delay_ms;

	rslt = bme280_init(&bme280);
	return rslt;
}

/**
 * @brief Read BME280 in normal mode
 *
 * Reads all BME280 values (Humidity, pressure, temperature) in normal mode
 * Values are set in comp_data pointer
 *
 * @param comp_data bme280 data structure
 * @return
 */
int8_t bme280_read_sensor_data_normal_mode(struct bme280_data *comp_data) {
	int8_t rslt;
	uint8_t settings_sel;

	/* Recommended mode of operation: Indoor navigation */
	bme280.settings.osr_h = BME280_OVERSAMPLING_1X;
	bme280.settings.osr_p = BME280_OVERSAMPLING_16X;
	bme280.settings.osr_t = BME280_OVERSAMPLING_2X;
	bme280.settings.filter = BME280_FILTER_COEFF_16;
	bme280.settings.standby_time = BME280_STANDBY_TIME_62_5_MS;

	settings_sel = BME280_OSR_PRESS_SEL;
	settings_sel |= BME280_OSR_TEMP_SEL;
	settings_sel |= BME280_OSR_HUM_SEL;
	settings_sel |= BME280_STANDBY_SEL;
	settings_sel |= BME280_FILTER_SEL;
	rslt = bme280_set_sensor_settings(settings_sel, &bme280);
	rslt = bme280_set_sensor_mode(BME280_NORMAL_MODE, &bme280);
	HAL_Delay(63);
	rslt = bme280_get_sensor_data(BME280_ALL, comp_data, &bme280);
	return rslt;
}
