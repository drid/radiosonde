/**
 * @file gps.c
 * @brief Brief_description
 *
 * @date May 7, 2020
 * @author Ilias Daradimos
 */

#include "gps.h"

/**
 * @brief Convert NMEA degrees to decomal degrees
 *
 * NMEA degres are provided in the form of DDMM.MMMM or DDDMM.MMMM \n
 * Decimal degrees are obtained using the formula
 * DD + MM.MMMM / 60
 * @param nmea_deg
 * @return Decimal degrees
 */
float gps_nmea_deg_to_float(char *nmea_deg) {
	float deg = 0;
	char *minpos;

	minpos = strchr(nmea_deg, '.');
	if (minpos != NULL) {
		deg = atof(minpos - 2) / 60;
		memset(minpos - 2, 0, 1);
		deg += atoi(nmea_deg);
	}
	return deg;
}

/**
 * @brief Parse NMEA RMC sentence
 * @param nmea_rmc String containing RMC data
 * @param gps_data
 */
void gps_parse_rmc(char *nmea, struct gps_data *gps_data) {
	char *ptr;
	uint8_t nmea_pos = 1;

	// remove CR
	ptr = strchr(nmea, '\r');
	if (ptr == NULL)
		return;
	*ptr = 0;

	while ((ptr = strsep(&nmea, ",")) != NULL) {
		switch (nmea_pos++) {
		case 1: //time
			memset(gps_data->time, 0, sizeof(gps_data->time));
			strncpy(gps_data->time, ptr, sizeof(gps_data->time));
			break;
		case 2: // Position validity
			gps_data->fix = (*ptr == 'A');
			if (!gps_data->fix)
				return;
			break;
		case 3: // Latitude
			gps_data->lat = gps_nmea_deg_to_float(ptr);
			break;
		case 4: // N/S
			if (*ptr == 'S')
				gps_data->lat *= -1;
			break;
		case 5: //Longitude
			gps_data->lon = gps_nmea_deg_to_float(ptr);
			break;
		case 6: // E/W
			if (*ptr == 'W')
				gps_data->lon *= -1;
			break;
		case 7: // ground speed in knots
			gps_data->speed = atof(ptr);
			break;
		case 8: // True course
			gps_data->true_course = atof(ptr);
			break;
		case 9: //Date
			memset(gps_data->date, 0, sizeof(gps_data->date));
			strncpy(gps_data->date, ptr, sizeof(gps_data->date));
			break;
		default:
			break;
		}
	}
}

/**
 * @brief Parse NMEA GGA sentence
 * @param nmea_rmc String containing RMC data
 * @param gps_data
 */
void gps_parse_gga(char *nmea, struct gps_data *gps_data) {
	char *ptr;
	uint8_t nmea_pos = 1;

	// remove CR
	ptr = strchr(nmea, '\r');
	if (ptr == NULL)
		return;
	*ptr = 0;

	while ((ptr = strsep(&nmea, ",")) != NULL) {
		switch (nmea_pos++) {
		case 9: // ground speed in knots
			gps_data->altitude = atof(ptr);
			return;
			break;
		default:
			break;
		}
	}
}

void gps2time(struct gps_data *gps_data, struct tm *time) {
	sscanf(gps_data->date, "%2i%2i%2i", &time->tm_mday, &time->tm_mon,
			&time->tm_year);
	sscanf(gps_data->time, "%2i%2i%2i", &time->tm_hour, &time->tm_min,
			&time->tm_sec);

	time->tm_year += 100;
	time->tm_mon -= 1;
}

