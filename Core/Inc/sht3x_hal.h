/**
 * @file sht3x_hal.h
 * @brief Brief_description
 *
 * @date Apr 18, 2020
 * @author Ilias Daradimos
 */

#ifndef INC_SHT3X_HAL_H_
#define INC_SHT3X_HAL_H_

#include <sht3x.h>
#include "stm32l1xx_hal.h"

extern I2C_HandleTypeDef hi2c2;

#define SHT3x_ADDRESS	0x44
#define SHT_Reset_Pin GPIO_PIN_14
#define SHT_Reset_GPIO_Port GPIOB
#define SHT_Alert_Pin GPIO_PIN_15
#define SHT_Alert_GPIO_Port GPIOB

void SHT3x_setup(struct sht3x_dev *sensor);

#endif /* INC_SHT3X_HAL_H_ */
